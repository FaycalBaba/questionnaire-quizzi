import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  url = "http://localhost:3000/api/submit";

  constructor(private http: HttpClient) { }



  public submit(questionnaireId: any, answers: any, email: string) {
    // console.log('submit()');

    const values = answers.map(q => {
      q = { ...q };

      if (q.type === 'DATE_TYPE') {
        q.answer = q.answer && q.answer.toString();
      }
      if (q.type === 'MULTI_CHOICE_SINGLE') {
        // console.log("submit " + JSON.stringify(q));

      }
      return q;
    });


    // let questionnaire = new Questionnaire();
    const payload = {
      questionnaire: questionnaireId,
      answers: values,
      submitter: email
    };
    return this.http.post(this.url, payload);
  }
}
