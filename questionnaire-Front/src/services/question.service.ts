import { Injectable } from '@angular/core';

import { MatDialogConfig, MatDialog } from '@angular/material';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Question } from 'src/models/models';
import { AddQuestionModalComponent } from 'src/app/profile/create/add-question-modal/add-question-modal.component';
import { catchError, map } from 'rxjs/operators';
import { QuestionnaireService } from './questionnaire.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  url: String = 'http://localhost:3000/api/profile/questionnaire/question';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  constructor(public dialog: MatDialog, private http: HttpClient, private router: Router) {

  }

  public addQuestion() {
    return this.openQuestion({});
  }

  public editQuestion(data) {
    return this.openQuestion(data);
  }

  private openQuestion(data?) {
    const config: MatDialogConfig = {
      width: '750px',
      data
    };
    const dialogRef = this.dialog.open(AddQuestionModalComponent, config);

    return dialogRef.afterClosed();
  }

  public getListQuestions(id): Observable<any> {
   // console.log('Question/getListQuestions(' + id + ')');
    return this.http.get(`${this.url}` + `/${id}`).pipe(map(res => {
      return res || {};
    }),
      catchError(this.handleError));
  }
}
