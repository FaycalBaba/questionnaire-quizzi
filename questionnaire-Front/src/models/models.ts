
export class UserInscription {
  name: string;
  email: string;
  birthdate: Date;
  password: string;

  constructor() {

  }

}

export class User {
  _id: string;
  email: string;
  name: string;
  exp: number;
  iat: number;
  constructor() {

  }
}

export interface TokenPayload {
  email: string;
  password: string;
  name?: string;
}

export enum QuestionType {
  Boolean,
  Multi_Multi,
  Multi_Single,
  Text
}

export enum EtatQuestionnaire {
  'Opened',
  'Shared',
  'Closed'
}

export class QuestionValue {
  value: string;
  answer: string;

  constructor(value, answer) {
    this.value = value;
    this.answer = answer;
  }
}


export class Questionnaire {
  _id: string;
  title: string;
  description: string;
  submittedBy: string;
  submittedById: string;
  dateCreate: Date;
  questions: Question[];
  Etat: EtatQuestionnaire;
  // nbrParticipation: number;
  dateLimite: Date;

  constructor(title = ' ', description = ' ', submittedBy = ' ', submittedById = ' ', questions = [], datelimite = null) {
    this.title = title;
    this.description = description;
    this.Etat = EtatQuestionnaire['Opened'];
    this.submittedBy = submittedBy;
    this.submittedById = submittedById;
    this.dateCreate = new Date();
    this.questions = questions;
    this.dateLimite = datelimite;
  }
}

export class Question {
  _id: string;
  title: string;
  type: string;
  // choix: string[];
  values: Array<any>;
  answer: string;

  constructor(title, type, values) {
    this.title = title;
    this.type = type;
    this.values = values;
    this.answer = '';
  }

}
