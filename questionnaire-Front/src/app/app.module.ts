import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { ReceptionComponent } from './reception/reception.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ProfileComponent } from './profile/profile.component';
import {
  MatIconModule,
  MatListModule,
  MatInputModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatRadioModule,
  MatTabsModule,
  MatSnackBarModule,
  MatNativeDateModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { ReceptionService } from './reception/reception.service';
import { DialogsService } from 'src/services/dialogs';
import { QuestionService } from 'src/services/question.service';
import { QuestionnaireService } from 'src/services/questionnaire.service';
import { ConfirmDialog } from 'src/shared/confirm-dialog.component';
import { AnswerComponent } from './answer/answer.component';
import { AddQuestionModalComponent } from './profile/create/add-question-modal/add-question-modal.component';
import { CreateComponent } from './profile/create/create.component';
import { QuestionTypeComponent } from './question-type/question-type.component';
import { ResponseComponent } from './profile/response/response.component';
import { ResponseDetailsComponent  } from './profile/response/response-details/response-details.component';



@NgModule({
  declarations: [
    AppComponent,
    ReceptionComponent,
    ProfileComponent,
    AddQuestionModalComponent,
    CreateComponent,
    QuestionTypeComponent,
    ConfirmDialog,
    AnswerComponent,
    ResponseComponent,
    ResponseDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    SlimLoadingBarModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BrowserModule,
    ReactiveFormsModule,
    // materil
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatRadioModule,
    MatTabsModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    SimpleNotificationsModule.forRoot()
  ],
  exports: [
    ConfirmDialog,
  ],
  entryComponents: [
    AddQuestionModalComponent,
    ConfirmDialog
  ],
  providers: [
    ReceptionService,
    DialogsService,
    QuestionService,
    QuestionnaireService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
