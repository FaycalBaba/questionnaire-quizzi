import { Component, OnInit } from '@angular/core';

import { NotificationsService } from 'angular2-notifications';

import { UserInscription, User, TokenPayload } from '../../models/models';
import { ReceptionService } from './reception.service';
import { NotificationConstants } from '../utils/notification.constants';
import { Router } from '@angular/router';


@Component({
  selector: 'app-reception',
  templateUrl: './reception.component.html',
  styleUrls: ['./reception.component.css']
})
export class ReceptionComponent implements OnInit {

  registerForm: UserInscription = new UserInscription();
  noCredentials: Boolean = false;
  credentials: TokenPayload = {
    email: '',
    password: ''
  };


  constructor(private receptionService: ReceptionService,
    private notificationsService: NotificationsService,
    private router: Router) {


  }

  ngOnInit() {

  }

  register() {
    // console.log("register component send request");
    this.receptionService.register(this.registerForm).subscribe(() => {
      // console.log("register component success");

      this.notificationsService.success(NotificationConstants.SUCCESS_MESSAGE, '', NotificationConstants.PROPERTIES);
      this.router.navigateByUrl('/profile');
    }, (err) => {
      this.notificationsService.error(NotificationConstants.ERROR_MESSAGE, '', NotificationConstants.PROPERTIES);
      // console.log("register component fail");

      console.error(err);
    });
  }

  login() {
    if (this.credentials.email != '' && this.credentials.password != '') {
      this.noCredentials = false;
     // console.log("Login click");

      this.receptionService.login(this.credentials).subscribe(() => {
        this.router.navigateByUrl('/profile');
      }, err => {
        console.error(err);
        this.notificationsService.error(NotificationConstants.ERROR_MESSAGE, '', NotificationConstants.PROPERTIES);
      });
    } else {
      this.noCredentials = true;
    }


  }
}
