import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseService } from 'src/services/response.service';
import { DialogsService } from 'src/services/dialogs';
import { Questionnaire } from 'src/models/models';

@Component({
  selector: 'app-response',
  templateUrl: './response.component.html',
  styleUrls: ['./response.component.css']
})
export class ResponseComponent implements OnInit {

  public submissionList: Observable<any>;
  goToDetails: Boolean;
  @Input() questionnaire: Questionnaire;
  answers: [any];
  @Output() goToProfilePage: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  detailsFinish(event: Boolean) {
    this.goToDetails = event;
  }

  constructor(private responseService: ResponseService,
    private dialogsService: DialogsService) { }

  goResponsePgae(event: Boolean) {
    this.goToDetails = event;
  }
  ngOnInit() {
    this.goToDetails = false;
    if (this.questionnaire) {

      this.responseService.get(this.questionnaire._id)
        .subscribe(response => {
          // console.log(response);
          this.submissionList = response;
        });
    }
  }

  getDetails() {
    this.submissionList.forEach(ele => this.answers = ele.answers);
    // console.log("getDetails ");
    this.answers.forEach(e => console.log(e));
    this.goToDetails = true;
  }


  remove(id) {
    this.dialogsService
      .confirm('Confirm Delete', 'Are you sure you want delete question?')
      .subscribe(res => res && this.responseService.remove(id));
  }

  goToProfile() {
    this.goToProfilePage.emit(false);
  }

}
