import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { ReceptionService } from './reception/reception.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {


  constructor(private receptionService: ReceptionService,
    private router: Router) { }

  canActivate() {
    if (!this.receptionService.isLoggedIn()) {
      this.router.navigateByUrl('/');
      return false;
    }
    return true;
  }
}
