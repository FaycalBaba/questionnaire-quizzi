## UPEC 2018/2019
#### RAPPORT PROJET Middleware
#### Questionnaire Quizzi

_Groupe_ :
&nbsp;&nbsp;&nbsp; MESKINE Mohamed - BABA Faycal

* Environnement de travail, Outils et FrameWork  :  
  &nbsp;&nbsp;&nbsp; plateforme serveur : Nodejs  
  &nbsp;&nbsp;&nbsp; Base de données : MongoDb  
  &nbsp;&nbsp;&nbsp; Editeur : Visual studio code  
  &nbsp;&nbsp;&nbsp; Frameword : express js , Angular 6, Material 6
  Bootstrap 4  
  &nbsp;&nbsp;&nbsp; outils : robo 3T  
  
   


 ### Lancer le projet
1. Clôner le projet puis 
lancer la commande Npm dans les racines des dossiers questionnaire-Front & questionaire-Back:
&nbsp;&nbsp;&nbsp;&nbsp; <pre> npm install </pre>
2. se placer dans la racine du dossier questionniare-Back et exécuter la commande
&nbsp;&nbsp;&nbsp;&nbsp; <pre> nodemon server </pre>
3. se placer dans la racine du dossier questionniare-Front et exécuter la commande
&nbsp;&nbsp;&nbsp;&nbsp; <pre> npm start </pre>

4. Accéder à l'url <pre> http://localhost:4200/</pre>

*remarque : Il faut lancer la npm install dans questionnaire-Front à partir de Git Bash.*
## Fonctionnalités :

* Service Authentification  
* Création de questionnaire avec plusieurs types de question
* Gestion des questionnaire
* Générer le PDF d'un questionnaire
* Générer l'url pour répondre à un questionnaire




