var Questionnaire = require('../models/questionnaire.js');
var express = require('express');
var responseRoutes = express.Router();
//var ensureAuthenticated = require('../config/passport').ensureAuthenticated;

responseRoutes.get('/:id', function (request, response) {
  var id = request.params.id;  
  Questionnaire.getResponseCount(id, function (err, results) {
    if (err) response.json(err);
    if (results) {
      response.json(results);
    }
    else{
      response.json(JSON.stringify({ "message": "pas de réponses" }));
      console.log("pas de réponses");
    }
  });
});

module.exports = responseRoutes;