
const express = require('express');
const _ = require('lodash');
var validator = require('express-form');
var field = validator.field;
var qstnrRoutes = express.Router();
// var ensureAuthenticated = require('../config/passport').ensureAuthenticated;

let Questionnaire = require('../models/questionnaire');
let Question = require('../models/question');

qstnrRoutes.get('/:id', function (request, response) {
  var id = request.params.id;

  console.log(id);

  Questionnaire.findById(id, function (err, questionnaire) {
    if (err) response.json(err);
    response.json(questionnaire);
  });
  /**find({
    _id: id
  }).populate('questions').exec(function (error, docs) {
    if (error) {
      response.send(error);
    } else {
      console.log(docs);
      var questions = docs[0] ? docs[0].questions : [];
      var title = docs[0] ? docs[0].title : 'Unnamed Survey';
      response.render('questionnaire', {
        questions: questions,
        id: id,
        titreQuestionnaire: title
      });
    }
  }); */
});


qstnrRoutes.get('/', function (request, response) {
  Questionnaire.find(function (error, questionnaires) {
    if (error) {
      console.log(error);
      response.render('questionnaires', {
        error: error
      });
      return;
    }
    response.json(questionnaires);
  });
});

qstnrRoutes.post('/addQstnr', function (request, response) {
  let questionnaire = new Questionnaire();

  questionnaire.title = request.body.title;
  questionnaire.submittedBy = request.body.submittedBy;
  questionnaire.submittedById = request.body.submittedById;
  questionnaire.description = request.body.description;
  questionnaire.dateCreate = request.body.dateCreate;
  questionnaire.dateLimite = request.body.dateLimite;

  request.body.questions.forEach(q => {
    let question = new Question();
    question.title = q.title;
    question.type = q.type;
    question.values = q.values;

    question.save();
    questionnaire.questions.push(question);

  });

  questionnaire.save()
    .then(game => {
      response.json(game);
    })
    .catch(err => {
      response.status(400).send("Erreur d'ajout de questionnaire" + err);
    });
});

qstnrRoutes.put('/:id', function (request, response) {

  var data = request.body;
  var id = request.params.id;
  var questions = [];
  var go = false;

  request.body.questions.forEach(q => {
    if (q._id) {
      console.log("qstnrRoutes/put " + q._id);
      Question.updateQuestion(q);
      questions.push(q);
      data.questions = questions;
    }
    else {
      console.log("qstnrRoutes/put " + q._id);
      let question = new Question();
      question.title = q.title;
      question.type = q.type;
      question.values = q.values;
      question.save();
      questions.push(question);
      data.questions = questions;
    }
  });
  console.log(" avant save qstnr" + JSON.stringify(data));
  Questionnaire.findByIdAndUpdate(id, data,
    function (err, questionnaire) {
      if (err) response.json(err);
      response.json(questionnaire);
    });
  /*findOneAndUpdate(id, data,
    function (err, questionnaire) {
      if (err) response.json(err);
      response.json(questionnaire);
    });
    update({
      _id: id
    }, data, function (error, numAffected, result) {
      if (error) {
        console.log('Erreur pendant la mise à jour du questionnaire');
      } else {
        response.send(result);
      }
    });*/

});

qstnrRoutes.delete('/:id', function (request, response) {
  var id = request.params.id;
  console.log("delete " + id);

  Questionnaire.findByIdAndRemove({ _id: id }, function (err, questionnaire) {
    if (err) response.json(err);
    else response.json(questionnaire);
  });
  //});
  /*update({
    _id: id
  }, function (eroor) {
    if (error) {
      response.send('Erreur de suppression de questionnaire');
    } else {
      response.send('Deleted');
    }
  });*/
});

module.exports = qstnrRoutes;