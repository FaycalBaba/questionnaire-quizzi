const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    userRoutes = require("./routes/user.route"),
    cors = require('cors'),
    receptionRoutes = require('./routes/user.route'),
    questionRoutes = require('./routes/question.route'),
    submissionRoutes = require('./routes/submission.route'),
    questionnaireRoutes = require('./routes/questionnaire.route'),
    responseRoutes = require('./routes/reponse.route');

var config = require('./config/db');
require('./config/passport');


app.use(cors());
app.use(bodyParser.json());
app.use('/api/profile/questionnaire/question', questionRoutes);
app.use('/api/profile/questionnaire', questionnaireRoutes);
app.use('/api/submit', submissionRoutes);
app.use('/api/reception', receptionRoutes);
app.use('/response', responseRoutes);
app.use('/api', userRoutes);
app.use(passport.initialize());


app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


mongoose.promise = global.Promise;
mongoose.connect(config.DB).then(
    () => {
        console.log("DB is connected");
    },
    err => {
        console.log("cannot connect to DB err:" + err);

    }
)

module.exports = app;
